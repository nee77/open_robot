if( window.app_views == undefined ) window.app_views = [];


window.app_views.push({
    // Главный вид
    main_view: {
        id: 'map1' // уникальный id вида
        ,menu: true // добавить вид в боковое меню
        ,text: 'Карта' // Текст в меню и в заголовке
        ,icon: 'globe' // Иконка в меню

        // Кнопки и элементы управления на верхней панели у вида
        ,tools: [
            {view: 'search', id: 'map1_search', placeholder: 'Поиск по карте', gravity: 3 }
            ,{width: 20}
            ,{view: 'button', type: 'iconButton', icon:'plus', label: 'Кнопка', id: 'map1_add_button', width: 120}
        ]

        // Основной вид модуля
        ,view: {
            id: 'map1_view' // Должен быть сделан из id вида (выше) с добавлением _view
            ,type: 'clean'
            ,rows: [
                //
                // Карта
                {
                    id: "map1_ymap",
                    view:"yandex-map",
                    center: [55.6000, 37.4000],
                    zoom: 15,
                    controls: ["zoomControl"],
                    mapType: 'yandex#satellite'
                }
            ]
        }
    }

    // Дополнительные окна в модуле
    ,viewInit: function(){

    }

    // Инициализация логики модуля
    ,logicInit: function(){

        var map_search = $$('map1_search');
        var add_button = $$('map1_add_button');
        var ymap1 = $$('map1_ymap');

        // Границы карты
        var limit_map_area = [[55.0000, 37.0000],[55.5000, 37.5000]];

        //
        // Отрисовка объектов на карте и добавление событий на них
        var map_init = function(){
            // При двойном клике карта зуммируется, отключаем эту функцию
            ymap1.map.events.add('dblclick', function (e) {
                e.preventDefault(); // При двойном щелчке зума не будет.
            });

            // Ограничение карты
            ymap1.map.options.set('restrictMapArea', limit_map_area);
        };


        //
        // После загрузки модуля карты начинаем работу с ней
        ymap1.attachEvent('onMapLoad', map_init);


        //
        // Добавить
        add_button.attachEvent('onItemClick', function () {
            webix.message('Add button');
        });

    }
});