webix.ready(function() {

    // Основные элементы на верхней панели
    var tools_data = [
        // Кнопка меню
        { view: "icon", icon: "bars", click: function(){
            if( $$("main_side_menu").config.hidden) $$("main_side_menu").show();
            else $$("main_side_menu").hide();
        }}
        // Поле для названия активного модуля
        ,{ view: "label", label: "", id: "top_toolbar_title" ,width: 260 }
    ];

    var menu_data = [];
    var main_view_cells = [];
    var views = window.app_views;

    // Перебираем модули, добавляем общие виды и меню
    for(var i = 0, k = views.length; i < k; i++){
        if( views[i].main_view ){
            // добавляем вид
            main_view_cells.push(views[i].main_view.view);

            // Если есть элементы управления, добавляем их в набор
            if( views[i].main_view.tools && views[i].main_view.tools.length ) {
                for (var n = 0, m = views[i].main_view.tools.length; n < m; n++) {
                    views[i].main_view.tools[n].tool_parent = views[i].main_view.id;
                    views[i].main_view.tools[n].hidden = true;
                    tools_data.push(views[i].main_view.tools[n]);
                }
            }

            // Добавляем в меню если нужно
            if( views[i].main_view.menu ){
                menu_data.push({
                    id: views[i].main_view.id
                    ,value: views[i].main_view.text
                    ,icon: views[i].main_view.icon
                });
            }
        }
    }

    // Запускаем главный вид
    webix.ui({
        rows: [
            // Верхняя панель с кнопкой вызова бокового меню
            { view: "toolbar", id: "top_toolbar", elements: tools_data}

            // Главный вид
            ,{ view: "multiview" ,id: 'main_view' ,animate: false ,cells: main_view_cells, fitBiggest: true }
        ]
    });

    // Функция переключения главного вида
    var set_main_view = function(view_id){
        // Найти вид в списке и установить его
        for(var i = 0, k = views.length; i < k; i++) {
            if (views[i].main_view && views[i].main_view.id == view_id) {
                // Пишем заголовок модуля
                $$('top_toolbar_title').setValue(views[i].main_view.text);

                // Переключаем элементы управления
                var child_views = $$('top_toolbar').getChildViews();
                if( child_views.length ){
                    // Сначала нужно выключить все
                    for( var p1 = 0, r1 = child_views.length; p1 < r1; p1++ ){
                        if( child_views[p1].config.tool_parent ){
                            child_views[p1].hide();
                        }
                    }
                    // Потом включить нужные
                    for( var p2 = 0, r2 = child_views.length; p2 < r2; p2++ ){
                        if( child_views[p2].config.tool_parent && child_views[p2].config.tool_parent == view_id ){
                            child_views[p2].show();
                        }
                    }
                }

                // Переключаем вид
                $$("main_view").setValue(view_id + '_view');

                $$('top_toolbar').refresh();
                break;
            }
        }
    };

    // Боковое меню
    webix.ui({
        view: "sidemenu",
        id: "main_side_menu",
        width: 250,
        position: "left",
        body:{
            view:"list"
            ,id: "main_side_menu_list"
            ,layout:"y"
            ,template: "<span class='webix_icon fa-#icon#'></span> #value#"
            ,data: menu_data
            ,on:{
                // При нажатии на пункт меню
                onItemClick:function(id){
                    $$("main_side_menu").hide();

                    set_main_view(id);
                }
            },
            borderless: true
        }
    });

    // Инициализация логики модулей в самом конце
    for(i = 0, k = views.length; i < k; i++){
        // Если есть функции для инициализации окошек
        if( views[i].viewInit ) views[i].viewInit();

        // А потом логика
        if( views[i].logicInit ) views[i].logicInit();
    }

    // Включение первого вида
    set_main_view(menu_data[0].id);

});